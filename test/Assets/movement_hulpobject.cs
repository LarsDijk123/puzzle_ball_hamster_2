using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement_hulpobject : MonoBehaviour
{
    public Transform targetObject;
    public Transform CameraObject;
    public float rotateforce = 10.0f;
    private void Awake()
    {
        targetObject.SetParent(null, true);
        CameraObject.localPosition = new Vector3 (0, 10, 15);
       

    }

    void FixedUpdate()
    {

        transform.position = targetObject.position;
        float x = 0.0f;
        if (Input.GetKey("left"))
        {
            transform.Rotate(0, -rotateforce, 0);
          
            
        }

        if (Input.GetKey("right"))
        {
            transform.Rotate(0, rotateforce, 0);
            
        }
        CameraObject.localPosition = new Vector3(0, 10, -15);
       
    }
}
